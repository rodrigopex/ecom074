#include "Oven.hpp"
#include <QDebug>

Oven::Oven(QObject *parent) :
    QObject(parent), m_ready(false), m_progress(0.0), m_internalTimer(new QTimer)
{
    m_internalTimer->setInterval(500);
    connect(m_internalTimer, &QTimer::timeout, this, &Oven::updateProgress);
    m_internalTimer->start();
}

Oven::~Oven()
{

}
double Oven::progress() const
{
    return m_progress;
}

void Oven::setProgress(double progress)
{
    if(m_progress != progress) {
        m_progress = progress;
        Q_EMIT this->progressChanged(m_progress);
        qDebug() << m_progress;
    }
}
bool Oven::ready() const
{
    return m_ready;
}

void Oven::setReady(bool ready)
{
    if(m_ready != ready) {
        m_ready = ready;
        Q_EMIT this->readyChanged(m_ready);
        qDebug() << "READY:" << m_ready;
    }
}

void Oven::updateProgress()
{
    if(static_cast<int>(m_progress) == 1) {
        m_internalTimer->stop();
        //disconnect(m_internalTimer, &QTimer::timeout, this, &Oven::updateProgress);
        this->setReady(true);
    } else {
        this->setProgress(m_progress + 0.1);
    }
}



