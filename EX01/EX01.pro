#-------------------------------------------------
#
# Project created by QtCreator 2015-05-06T10:06:44
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = EX01
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Oven.cpp

HEADERS += \
    Oven.hpp
