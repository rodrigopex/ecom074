#ifndef OVEN_H
#define OVEN_H

#include <QObject>
#include <QTimer>

class Oven : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool ready READ ready WRITE setReady NOTIFY readyChanged)
    Q_PROPERTY(double progress READ progress WRITE setProgress NOTIFY progressChanged)
public:
    explicit Oven(QObject *parent = 0);
    ~Oven();
    double progress() const;
    void setProgress(double progress);
    bool ready() const;
    void setReady(bool ready);
signals:
    void readyChanged(bool ready);
    void progressChanged(double progress);

public slots:
private slots:
    void updateProgress();
private:
    bool m_ready;
    double m_progress;
    QTimer *m_internalTimer;
};

#endif // OVEN_H
