#include <QCoreApplication>
#include "Oven.hpp"
#include <QSharedPointer>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QSharedPointer<Oven> oven(new Oven);
    return a.exec();
}
