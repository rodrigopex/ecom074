#include <QCoreApplication>
#include "Car.hpp"
#include <QDebug>

void testPrint() {
    qDebug() << Q_FUNC_INFO << "Hello!";
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Car * c = new Car(&a);
    c->setName("Celta");
    qDebug() << Q_FUNC_INFO << c->property("name").toString();
    qDebug() << Q_FUNC_INFO << c->property("name");
    qDebug() << Q_FUNC_INFO << c->name();
    c->setProperty("rodas", QVariant::fromValue(static_cast<float>(4)));
    qDebug() << Q_FUNC_INFO << c->property("rodas");

    return a.exec();
}

