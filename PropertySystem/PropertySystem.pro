QT += core
QT -= gui

TARGET = PropertySystem
CONFIG += console \
          c++11
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    Car.cpp

HEADERS += \
    Car.hpp

