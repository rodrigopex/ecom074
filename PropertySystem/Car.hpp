//#ifndef CARRO_H
//#define CARRO_H

#pragma once

#include <QObject>


class Car : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int rodas MEMBER m_rodas NOTIFY rodasChanged)
    Q_PROPERTY(int aro READ aro WRITE setAro NOTIFY aroChanged)
    Q_PROPERTY(bool azul MEMBER m_azul NOTIFY azulChanged)
public:
    explicit Car(QObject *parent = 0);

    QString name() const;
    void setName(const QString &name);

    int aro() const
    {
        return m_aro;
    }

signals:
    void nameChanged(const QString &name);
    void rodasChanged(int rodas);

    void aroChanged(int aro);

    void azulChanged(bool azul);

public slots:
void setAro(int aro)
{
    if (m_aro == aro)
        return;

    m_aro = aro;
    emit aroChanged(aro);
}

private:
    QString m_name;
    int m_rodas;
    int m_aro;
    bool m_azul;
};

//#endif // CARRO_H
