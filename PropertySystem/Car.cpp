#include "Car.hpp"

Car::Car(QObject *parent) : QObject(parent)
{

}

QString Car::name() const
{
    return m_name;
}

void Car::setName(const QString &name)
{
    if(m_name != name) {
        m_name = name;
        emit this->nameChanged(name);
    }
}

