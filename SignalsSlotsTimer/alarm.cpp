#include "alarm.h"
#include <QDebug>

Alarm::Alarm(QObject *parent) : QObject(parent)
{
    m_count = 0;
    m_buzzer = new Buzzer(this);
    connect(this, &Alarm::playBuzzer, m_buzzer, &Buzzer::play);
}

Alarm::~Alarm()
{

}

void Alarm::playSound()
{

//    if(m_count == 3) {
//        //disconnect(this, &Alarm::playBuzzer, m_buzzer, &Buzzer::play);
//    }
    emit this->playBuzzer();
    m_count++;
    qDebug() << ".";
}

