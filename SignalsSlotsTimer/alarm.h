#ifndef ALARM_H
#define ALARM_H

#include <QObject>
#include "buzzer.h"

class Alarm : public QObject
{
    Q_OBJECT
public:
    explicit Alarm(QObject *parent = 0);
    ~Alarm();

signals:
    void playBuzzer();

public slots:
    void playSound();
private:
    int m_count;
    Buzzer *m_buzzer;
};

#endif // ALARM_H
