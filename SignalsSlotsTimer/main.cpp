#include <QCoreApplication>

#include <QTimer>
#include "alarm.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    Alarm *a = new Alarm;
    a->setParent(&app);

    QTimer *t = new QTimer(a);
    t->setInterval(3000);

    QObject::connect(t, &QTimer::timeout, a, &Alarm::playSound);

    t->start();

    return app.exec();
}
