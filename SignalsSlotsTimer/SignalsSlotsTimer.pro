#-------------------------------------------------
#
# Project created by QtCreator 2015-04-22T10:07:48
#
#-------------------------------------------------

QT       += core multimedia qml

QT       -= gui

TARGET = SignalsSlotsTimer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    alarm.cpp \
    buzzer.cpp

HEADERS += \
    alarm.h \
    buzzer.h
