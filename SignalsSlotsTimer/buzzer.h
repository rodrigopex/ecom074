#ifndef BUZZER_H
#define BUZZER_H

#include <QObject>

class Buzzer : public QObject
{
    Q_OBJECT
public:
    explicit Buzzer(QObject *parent = 0);
    ~Buzzer();

signals:

public slots:
    void play();

};

#endif // BUZZER_H
