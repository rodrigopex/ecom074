import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

Window {
    id: ui
    visible: true
    width: 600
    height: 300
    Item {
        anchors.fill: parent
        anchors.margins: 20
        FileDialog {
            id: fileSource
            onAccepted: {
                console.log("You chose: " + fileSource.fileUrl)
            }
        }
        FileDialog {
            id: fileDestination
        }
        ColumnLayout {
            anchors.fill: parent
            RowLayout {
                Label {
                    text: "File"
                }
                TextField {
                    Layout.fillWidth: true
                    text: fileSource.fileUrl
                    readOnly: true
                }
                Button {
                    text: "..."
                    onClicked: fileSource.open()
                }
            }
            CheckBox {
                id: same
                text: "Same folder, same name"
            }
            RowLayout {
                enabled: !same.checked
                Label {
                    text: "Destination"
                }
                TextField {
                    Layout.fillWidth: true
                }
                Button {
                    text: "..."
                    onClicked: fileDestination.open()
                }
            }
            Button {
                text: "Compress"
                Layout.alignment: Qt.AlignRight
            }
        }
    }
}

