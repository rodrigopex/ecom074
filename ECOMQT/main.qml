import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4


Window {
    id: ui
    visible: true
    width: 800
    height: 600
    function mm(value) {
        return Screen.pixelDensity * value
    }
    RowLayout {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 0
        Rectangle {
            color: "green"
            Layout.minimumWidth: ui.mm(50)
            Layout.preferredHeight: ui.mm(40)
            Rectangle {
                color: "lightgreen"
                anchors.fill: parent
                anchors.margins: 20
            }
        }
        Rectangle {
            id: textSession
            color: "#E0E0E0"
            Layout.fillHeight: true
            Layout.fillWidth: true
            ColumnLayout {
                anchors.fill: parent
                Rectangle {
                    color: "#f6f6f6"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    ListView {
                        id: messageList
                        anchors.fill: parent
                        model: [1,2,3]
                        delegate: Text{
                            text: modelData
                        }
                    }
                }
                Rectangle {
                    color: "yellow"
                    Layout.preferredHeight: parent.height*0.1
                    Layout.fillWidth: true
                    RowLayout {
                        anchors.fill: parent
                        spacing: 0
                        TextArea {
                            id: message
                            function send() {
                                if(message.text) {
                                    console.log(message.text)
                                    message.text = ""
                                }
                            }
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            Keys.onReturnPressed: send()
                        }
                        Button {
                            Layout.preferredWidth: parent.width*0.2
                            Layout.fillHeight: true
                            text: "Send"
                            onClicked: {
                                message.send()
//                                messageList.model.push(4)
                            }
                        }
                    }
                }
            }
        }
    }
}

