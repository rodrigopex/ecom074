#include <QCoreApplication>
#include "test.h"

#include <QSharedPointer>

int main()
{
    QSharedPointer<Test> t(new Test("T1", 0));
    {
        QWeakPointer<Test> t1 = t;
        Test * t2 = new Test("T2", t1.data());
        Test * t21 = new Test("T21", t2);
        Test * t22 = new Test("T22", t2);
        Test * t3 = new Test("T3", t1.data());
        Test * t31 = new Test("T31", t3);
        qDebug() << "------";
    }
    QSharedPointer<Test> t4 = t;
    return 0;
}
