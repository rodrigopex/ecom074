#-------------------------------------------------
#
# Project created by QtCreator 2015-04-15T10:05:24
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = HelloQObject
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    test.cpp

HEADERS += \
    test.h
