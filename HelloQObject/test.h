#ifndef TEST_H
#define TEST_H
#include <QDebug>
#include <QObject>

class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QString name, QObject * parent = 0);
    ~Test();
 };

#endif // TEST_H
